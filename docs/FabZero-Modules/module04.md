# 4. Découpe assistée par ordinateur

Cette semaine on va apprendre à utiliser les découpeuses laser.

### Utilisation des machines

Tout d'abord, quelques règles à prendre en compte lors de l'utilisation des machines. Il faut d'abord prendre en compte que le fichier que l'on possède sur l'ordinateur doit être vectoriel pour réaliser la découpe, de préférence des fichiers SVG. Le principe d'un fichier vectoriel est que les tracé ne perde pas en qualité peu importe à quel point on zoom dessus. Que l'on peut différencié avec un dessin matriciel qui constitué de pixels.

### Les matériaux

**Recommandés**

-bois contreplaqué de 3 ou 4 mm idéalement

-carton

-papier

-acrylique

-certains textiles


**Déconseillés**

-MDF

-ABS, PS

-les métaux

-polyétylène épais PE, PET, PP

-les matériaux composites à base de fibre


**Interdits**

-Téflon

-le PVC

-le cuivre

-le vinyle ou le simili-cuir

-Résine phénolique, époxy


### Les machines


_Machine 1 : Epilog Fusion Pro 32_
Il s'agit d'une machine conçue en usine. Elle est bien plus petite que la Lasersaur donc utilisée plus pour des petites pièces et principalement de la gravure. Elle a une surface de travail de 50x30cm et un laser de 40 watt.

![](../images/fusionpro.jpg)

Ci-joint, [le manuel élaboré par le FabLab pour l'utilisation de l'EpilogFusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md).


_Machine 2 : Lasersaur_
Il s'agit d'une machine en open-source que le FabLab a construit lui-même. Elle permet de travailler sur une plus grande surface que la première machine et faire des découpes de qualité de par sa puissance. Avec un espace de découpe de 122x61 cm et une hauteur  de 12 cm. Elle peut monter a une vitesse maximale de 6000 mm/min. La puissance de son laser est de 100 Watt infrarouge. Cette interface gère les fichier SVG ou DXF. Avec une préférence pour les fichiers SVG. S'il y a un problème, on peut passer sur INKSCAPE (c'est la version gratuite en terme de logiciel vectoriel) pour le régler.


![](../images/lasersaur.jpg)


Ci-joint, [le manuel élaboré par le FabLab pour l'utilisation du Lasersaur en toute sécurité](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md).


## Première découpe : le calibrage

Le première exercice pour nous familiariser avec les machines et de savoir comment la paramétrer pour avoir le résultat qu'on recherche.

##### Découpe

Pour le calibrage de la découpe, on a récupéré [ce fichier de base](../images/calibration_grid.svg)
Puis il a fallu paramétrer chacune des couleurs de découpe selon la graduation défini. Pour exemple, le premier carré rouge ci-dessous doit être définis avec une vitesse de 10% pour une puissance de 4%.

![](../images/calibration-grid.jpg)

![](../images/calibration_decoupe.jpg)


##### Pliage

Pour le pliage, on créé un [dessin](../images/module-04/calibration_grid_pliage.svg)  sur le même même principe. Seule différence, on a garder une vitesse identique 75% et on a modulé la puissance allant de 10 à 55%.

![](../images/calibration-grid-pliage.png)

![](../images/pliage.jpg)



## La lampe 

L'exercice suivant propose de réaliser une lampe. Cette lampe devait être réaliser sur une feuille polypropylène, un plastique translucide, et sans autre matière. Autre contrainte, avoir le minimum de perte de matière.

### Esquisses et test papier

Pour minimiser les pertes, j'ai décidé d'utiliser des formes géométriques orthogonales. 
L'idée est de créer une tour où passera un tube néon surlaquelle viennent se fixer des carrés et créer un "tourbillon" de carré. Le carré du milieu fera 10x10cm et les 2 aux extrémités 5x5cm, cela créera un élément visuellement plus attractif.

Voici un schéma conceptuel et un test papier: 

![](../images/tourbillon.png)


![](../images/modèlepapier.jpg)



### Dessin et découpe laser


J'ai dessiné le modèle de découpe sur autocad, je l'exporte en format dxf, que j'importe ensuite sur Inkscape afin de créer un fichier svg pour la découpe.  *[Cliquez ici](../images/lampe-découpeuse-laser.svgz)* pour télécharger le fichier svg du modèle de la lampe. 


![](../images/autocad.jpg)


Pour configurer ma découpe j’attribue à chaque « passage » une couleur qui correspond à un claque et j’attribue à chacun une valeur de vitesse et de puissance du laser a l’aide des tests de découpe et de plis réalisé précédemment. Cela définira si mes traits sont des gravures, traits de découpe ou des entailles pour un pliage. 

Pour le calque « gravure légère » je choisis 800 mm/min pour 4% de puissance (blanc)
Pour le calque « gravure profonde » je choisis 800 mm/min pour 10% de puissance (bleu)
Pour le calque « pliage » je choisis 400 mm/min pour 50% de puissance (orange)


Après avoir préparé le fichier .svg , je calibre la hauteur du laser grâce à une pièce en plastique prévu pour ça, et place ma feuille que je bloque avec des pièces en métal en dehors de la zone de découpe.
La fonction move du software permet de prévisualiser l’emplacement du laser pour le début de la découpe et donc d’économiser du matériel en placent la découpe dans un coin de la feuille.


![](../images/plaque.jpg)



L'interface machine s'ouvre alors et on peut commencer à paramétrer l'ensemble. Il faut d'abord séléctionner 'pluger' ou 'palpeur' en français. Ensuite éclater le dessin par calque de couleur en cliquant sur le ''+ couleur''. Après il faut veiller à programmer le bon process, ici 'vector' ou 'découpe' en français pour la découpe. Pour paramétrer la puissance et la vitesse, on se réfère au test de calibrage fait au préalable. Ici, j'ai choisi une vitesse de 10% pour une puissance de 20% et en ajustant la fréquence à 100%. Ces paramètres été choisit pour chacun des 3 calques. Ensuite on les met dans l'ordre de découpe. Enfin on peut imprimer.


![](../images/inkscape.jpg)



Voici un apercu de la lampe en cours

![](../images/demilampe.jpg)


![](../images/demilampe2.jpg)


![](../images/testlumière.jpg)



## Liens utiles

- [Télécharger Inkscape](https://inkscape.org/release/inkscape-1.1.1/)
- [Site Lasersaur](https://www.lasersaur.com)
- [Guide Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
- [Wiki réglages par matériaux Lasersaur](https://github.com/nortd/lasersaur/wiki/materials)
-[Site Epilog](https://www.epiloglaser.com)
- [Guide Epilog Fusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)
- [Wiki réglages par matériaux Epilog](https://github.com/nortd/lasersaur/wiki/materials)
- [Télécharger fichier svg de la lampe](../images/lampe-découpeuse-laser.svgz)


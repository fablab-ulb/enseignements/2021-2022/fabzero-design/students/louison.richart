# 2. Fusion 360

## Cube FabLab

Cette semaine, nous avons appris à prendre en main **Fusion 360**, un logiciel de de conception et rendu 3D. Je me suis vite rendu compte que ce logiciel était assez similaire à Autocad et Sketchup : des logiciels que je connais déjà bien. 

L’exercice est de réaliser le cube du logo du Fablab.  

La première chose à faire est évidement de télécharger le programme qui se fait sans aucun problème sous Windows 10. 

On ouvre le logiciel et on clique sur _« Créer une esquisse »_. On choisit ensuite sur quel plan on souhaite travailler. Ici : plan du bas. 
Cela va nous permettre de travailler en 2D. 

**Important** : on commence toujours à travailler par l’origine. 


![](../images/a.jpg)


On créé donc un rectangle par le centre de 100x100 qu’on va ensuite extruder via la touche **« E »** du clavier. 

On a plusieurs choix d’extrusion : 
- par le haut
- par le bas 
- du milieu vers l’extérieur). 

Ici : on extrude le carré de façon symétrique en partant de l’origine et en allant vers le haut et le bas, d’une hauteur totale de 100mm. On obtient un cube. 


![](../images/b.jpg)


#### Percement de trous dans ce cube. 

Pour se faire, on resélectionne _« Esquisse »_ et on choisit une face sur laquelle on va travailler. On clique sur **« C »** pour créer un cercle, **, _« double-clique »_ sur le plan sélectionné et **« D »** pour lui donner une dimension. Ici : 50.


![](../images/c.jpg)


![](../images/d.jpg)


Puis, on extrude (E) la surface intérieure du cercle. Il y a une petite flèche nous indiquant dans quel sens on veut extruder.

Ici, on note -100 pour retirer la matière dans toute la longueur du cube. On obtient un cube percé d’un cylindre en son milieu. 


![](../images/e.jpg)



#### Trouer un 2e cylindre dans le cube

On clique sur _« Créer »_, _« Réseau »_ et enfin _« Réseau circulaire »_. 

Une petite page s’ouvre sur la droite de notre écran. Pour recréer ce cylindre, on choisit donc de travailler avec _« Fonction »_, on clique sur la surface que l’on veut travailler (le cylindre) et on clique enfin sur l’axe qui nous intéresse. 

Ici : soit l’axe bleu ou rouge (-> bleu). On choisit également de travailler en _« angles »_, avec _« 90° »_ et avec une quantité de _« 2 »_. 

On réplique en fait le même cylindre mais à 90° du premier. 


![](../images/f.jpg)


![](../images/g.jpg)


#### Trouer le 3e cylindre dans le cube

On répète la même opération mais avec l’axe rouge (ou le dernier axe pas encore utilisé pour créer la dernière extrusion cylindrique). 

On obtient un cube percé de cercles sur 6 faces en leur milieu. 


#### Extruder l’espace entre les trous

Pour cette partie, nous devons à nouveau changer la caméra sur la face avant du cube. 
J'ai commencé par créer un rectangle aléatoire (la taille sera ajustée juste après), en indiquant le bord du carré et en le faisant glisser vers le milieu de l'axe vert. À ce stade, les bords incurvés sont visibles, mais cela peut être ignoré car nous nous concentrons sur le carré du milieu avec le cylindre déjà enfoncé.
Comme le rectangle est entièrement aléatoire, nous devrons sélectionner la ligne rouge (comme indiqué ci-dessus) ET le point central. Cliquez enfin sur _« Contraintes »_ puis _« MidPoint »_. De cette façon, les deux s'alignent au milieu.

Ici: la distance sélectionnée est de 25 mm. Et vous pouvez finaliser en extrudant vers l'arrière.


![](../images/h.jpg)


![](../images/i.jpg)


À ce stade, les 2e et 3e côtés peuvent également être extrudés, en insérant un axe. 

Pour cela, quittez d'abord la zone d'esquisse puis on va dans _« Construire »_ puis dans _« Axe passant par 2 points »_. 
Ensuite, choisissez deux points du cube sur lesquels vous souhaitez que l'axe aille.  



Toujours en dehors de la zone de croquis, continuez en allant dans _"Créer"_, _"Motif"_ et _"Motif circulaire"_.

J'ai sélectionnais la pièce extrudée, en cliquant sur les 2 faces, dans "Objets". L'axe que nous voulons est celui que nous venons de créer : Sélectionnez l'Axe bleu dans "Axe". Procédez ensuite à la sélection de la quantité 3.

En cliquant sur "OK", Fusion 360 devrait pouvoir extruder automatiquement la dernière étape.


![](../images/j.jpg)


![](../images/k.jpg)


#### Arrondir les côtés

Pour se faire, on utilise l’outil en bas à gauche qui nous permet de revenir en arrière dans la conception de l’objet sans effacer le travail. 

On revient donc à l’étape du cube. 


![](../images/m.jpg)


On va dans _ « Modifier » puis _ « Congé ». (ou directement la touche F)

On sélectionne tout le cube et on encode 10mm. 


![](../images/n.jpg)


Et on revient finalement à la fin.

_**Voilà notre cube FabLab !!!!!**_


![](../images/o.jpg)










## Objet

Une fois cela pris en main, il a été facile de créer mon objet (choisi dans le musée du Design de Bruxelles)

Il s’agit d’un porte-revus de _Giotto Stoppino_.


![](../images/porterevue.jpg)


La première étape est de créer un rectangle de 400X45mm. 
J’ai ensuite arrondi les bords de ce rectangle en utilisant l’outil **Cercle de 3 points** en allant dans _Créer_ puis _Cercle_. On extrude (E) d’une hauteur de 240mm.
On obtient facilement cela : 


![](../images/volume.jpg)


J’ai ensuite cliqué sur la surface supérieure et créer une esquisse dessus pour la modifier. 

J’ai utilisé l’outil **Décalage** qui m’a permis de créer une bordure de 5mm et j’ai finalement extrudé la partie centrale de la surface de -235mm pour également créer un fond à l’objet. 


![](../images/decalage.jpg)



![](../images/element1.jpg)


Après, j’ai sélectionné l’ensemble de l’objet 1 pour le décupler en 2 autres exemplaires et côte à côte. 
Pour cela, on va dans _Réseau_ puis _réseau circulaire_ . 
On choisit donc 3 quantités avec une distance de 80mm pour que les couches se superposent.  


![](../images/moitiéobjet.jpg)


Pour effectuer un décalage en hauteur, j’ai sélectionné le corps 3 et je lui ai demandé de monter de 100mm sur l’axe Z.
 Même opération pour élever le corps 2 de 50mm. 


![](../images/decalagehauteur.jpg)


Une fois cela fait, il ne restait plus qu’à aller dans **Système miroir** pour effectuer une symétrie des 3 corps liés. Il faut bien spécifier _Joindre_ si l’on veut que les 2 morceaux ne fassent qu’un. 


![](../images/moitiéobjetdécalé.jpg)


Voilà le résultat final :


![](../images/objetfini.jpg)



## **Liens utiles**

- [Fusion360](https://www.autodesk.com/products/fusion-360/personal)
- [Tutoriel sur les paramètres sur Fusion360](https://www.youtube.com/watch?v=90bvJHHCd24)
- [Tutoriel 'imbedded model'](https://www.youtube.com/watch?v=lWDh7XZASoA)
- [Télécharger le modèle format .stl](../images/Porte-revue_v1.stl)


# 1. Documentation

Cette semaine, j'ai appris à utiliser **GitLab**, une plateforme qui va me permettre de créer mon propre espace de travail où toutes mes démarches y seront expliquées et mes travaux y seront répertoriés. Cet outil va permettre de faire voir aux autres mon travail et mon évolution. Il me permet également de revoir mes démarches en permanence.

Il m'a cependant posé des problèmes mais dû à un manque de compréhension.

Lors de ce module 1, nous avons donc suivi une première formation donné par Denis Terwagne.


## Configurer Git

Dans un premier temps, il a été nécessaire d’installer Shell Bash pour faire fonctionner tout ça. La fusion de Bash et Git est appelée **Git BASH**. Shell Bash se trouve par défaut sur les ordinateurs sous Windows 10, mais c’est une fonction cachée qu’il faut activer.

Pour les ordinateurs comme le mien, fonctionnant sous Windows 10 : aller dans “Paramètres” > “Mise à jour et sécurité” et passer en mode “Pour les développeurs”. La première étape est terminée.
Dans l’application “Fonctionnalité”, dans “Activer ou désactiver des fonctionnalités Windows” il va falloir cocher la case “Sous-système Windows pour Linux” puis attendre que l’ordinateur se redémarre pour être fonctionnel.
Une fois l’ordinateur rallumé, ouvrez le terminal de commande et tapper les commandes suivantes (voir photo) pour s’assurer que tout à fonctionné.


![](../images/demarrage.jpg)


Vous venez de créer **Git BASH**, il sera nécessaire de le configurer en s’identifiant directement via le terminal avec son nom et son e-mail (voir photo ci-dessous).

![](../images/etape1.jpg)

## Clé ssh

Pour connecter mon ordinateur à GitLab, j’ai du ajouter des informations de sécurité pour m’authentifier, en plus de configurer mes identifiants.
Je décide donc de m'identifier via une clé SSH. Une fois créée, Gitlab ne me demandera plus de m'identifier.

La clé se trouve en .ssh dans un dossier sur votre ordinateur. (ici: le format ed25519 mais d’autres formats existent).


Pour la créer, rentrez : 

_ssh-keygen -t ed25519 -C "comment"_

J'ai remplacé "comment" par "creation clé ssh" pour l'identifier plus facilement et je n'ai pas mis de mots de passe (il faut laisser vide et appuyer sur entrée deux fois lorsqu'il est demandé une passphrase dans ce cas) et celle-ci est alors créée :


![](../images/clessh.png)


![](../images/clessh2.jpg)


## Clonage


La dernière étape de configuration du GitLab est de créer une copie locale de votre référentiel distant sur votre ordinateur. Cela nous permet de travailler sur notre PC directement depuis un éditeur de textes plutôt qu'en ligne sur le GitLab, tout en ayant les deux reliés.

Premièrement, sur votre projet dans le GitLab, "louison.richart" dans mon cas, vous trouverez le code à copier pour un clone par SSH dans l'onglet "Clone".

Vous ouvrez un terminal depuis le dossier où vous voulez que la copie locale se situe et entrez la commande ci-dessous en remplaçant le dernier élément par celui copié sur votre projet dans l'étape précédente :

git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/louison.richart.git


![](../images/clonage.jpg)


_Attention:_ s'il vous demande une confirmation comme sur la copie d'écran ci-dessous, il faut bien rentrer "yes" puis appuyer sur entrée, car si comme moi vous appuyez uniquement sur entrée, le clone ne sera pas effectué. 


## Atom


Le premier exercice de documentation consistait à créer notre page de présentation.
Pour documenter, vous pouvez maintenant soit le faire depuis votre ordinateur grâce au clone effectué, ou depuis le GitLab.
Depuis mon ordinateur, j'utilise le logicel d'édition de texte Atom pour modifier les fichiers "*.md*" qui est assez facile à prendre en main.

Je l'ai téléchargé [ici](https://atom.io).

Une fois Atom installer, il suffit de l'ouvrir et sur macbook aller sur le coin gauche en haut 'file' et cliquer que 'add project folder'.


![](../images/atom.jpg)


Choisir son fichier et cliquer sur ouvrir. On a alors accès à tous les modules de GitHub.


## Push/Pull/Fetch

Une fois le texte est écrit il faut savoir les synchronisé et pour ca nous avons besoin de c'est 3 mots clefs FETCH, PULL et PUSH.

**FETCH :** Permet d'être sur que nous somme bien sur la dernière version.

**PULL:** Permet de récupérer la dernière version en ligne.

**PUSH:** Permet d'envoyer les modifications faites en local c'est-a-dire sur Atom.

On constate que les fichiers modifier s'affichent à droite en jaunes avec un cercle remplis et les images ajoutés en vert avec un + .
On clique sur _"stage all"_

Tout se déplace automatiquement, on peut ajouter un commentaire pour s'y retrouver et puis: _"commit to main"_

![](../images/atom2.jpg)


Il suffit ensuite juste de cliquer sur _"push"_ pour que les textes se synchronise sur le site. 


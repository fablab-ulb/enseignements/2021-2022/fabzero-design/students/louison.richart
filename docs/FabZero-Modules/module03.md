# 3. Impression 3D


Cette semaine, notre troisième module s'articulait autour de l'impression 3D. Au FabLab, on trouve plusieurs imprimantes 3D Prusa : des modèles i3 MK3 et i3 MK3S/MK3S+. Afin de prendre en main leur utilisation et la préparation sur le logiciel au préalable, nous avions pour exercice de réaliser notre objet ou une partie de l'objet réalisé au module 2 sur Fusion 360 en taille réduite si nécessaire et de façon à ce que l'impression dure moins de 2 heures.

**L'imprimante Prusa**

Leur principe d'entreprise était de créer des machines open-source, ainsi bon nombre de leurs composants sont imprimables par d'autres imprimantes 3D : tous les codes sources, pièces imprimées, plans, conceptions de cartes de circuits imprimés etc sont disponibles gratuitement grâce au GitHub, et la communauté partage son savoir sur [PrusaPrinters.org](https://www.prusaprinters.org/#_ga=2.235507273.994300260.1634330638-22385276.1634330638).


Pour débuter l'impression d'un objet, il faut télécharger [Prusa](https://www.prusa3d.com/drivers/).
Ce logiciel permet de paramétrer les épaisseurs, remplissages, supports et autres réglages.

## Paramétrage sur le logiciel PrusaSlicer

Dans un premier temps, il suffit de convertir le fichier Fusion360 de votre objet en fichier *.STL* et de l'ouvrir sur PrusaSlicer.

Une fois sur le logiciel, il faut mettre l'objet à plat sur le plateau. La commande _"Place on Face"_ (F) permet ça en un clic : il suffit de cliquer sur la face que l'on veut mettre à plat. Ici, je travaille verticalement. 


![](../images/prusa1.jpg)


Pour réduire le temps d'impression de ce premier prototype de test, j'ai réduit l'échelle de mon objet à 12% dans le menu latéral. En réduisant l'échelle directement sur PrusaSlicer, les proportions sont conservées.

![](../images/echelle.png)

Dans les réglages généraux de ce même menu, je sélectionne le paramètre d'impression correspondant à mon imprimante, le filament choisi (ici, Prusament PLA) et le type d'imprimante. 


![](../images/paramètresprusa.png)


Conseil : se mettre en mode "Expert" pour avoir tous les paramètres.


Dans l'onglet _"Réglages d'impression"_, je règle tout d'abord les paramètres de _"couches et périmètres"_. Sachant que mon objet est un test et qu'il est de très petite taille, je règle le périmètre des couches verticales et horizontales sur 2 (par défaut le réglage est souvent de 3), si l'on veut le rendre plus solide ou le rendre étanche il faut augmenter ces couches. En choisissant les mêmes paramètres pour les deux, l'objet sera d'épaisseurs homogènes.


![](../images/paramètresprusa2.png)


Je règle le remplissage en rectiligne et de 10% car une fois de plus, l'objet est de petite taille et ne nécessite pas une grande solidité. Pour le rendre plus solide, on peut augmenter son pourcentage de remplissage et opter pour un motif plus compact ou résistant. 



Je crée également un support pour mon objet : cela va créer un support plus large à la base de mon objet qui sera détachable mais qui permet d'augmenter sa surface d'adhérence au plateau chauffant et éviter qu'il bouge ou se renverse. Vous pouvez le voir en vert sur le rendu avant impression. 


![](../images/support.jpg)


Une fois tous les réglages effectués, je clique sur _"Découper maintenant"_ et je peux voir le temps d'impression estimé :  1h46


J'exporte enfin le fichier en G-code que je vais mettre sur une carte SD d'une imprimante.


## Réaliser l’objet sur l’imprimante 3D


![](../images/imprimante.jpg)


Avant toute chose il est important de nettoyer votre espace de travail, à l’aide du produit et de papier mis a disposition, nettoyer la plaque de l’imprimante 3D pour oter toute substance qui pourrait empecher la matière d’accrocher au plateau.

Pour introduire votre fichier, inserez la carte SD dans  sur le coté gauche de l’imprimante et sélectionner le fichier à votre nom.
Une fois que l’imprimante sera à bonne température, l’impression débutera. 


![](../images/infoimprimante.jpg)


Il faut aussi vérifier également que l'impression débute bien à une hauteur Z de 0,2. 

_Conseil : il est toujours préférable de rester surveiller l'impression pour les 3 premières couches afin de vérifier que tout se passe bien et que nos paramètres sont bons._


![](../images/premierescouches.jpg)



Une fois l’impression terminée, je laisse refroidir le plateau un instant puis je l’enlève de son support.

Le plateau est légèrement souple ce qui vous permettra de décoller votre objet sans problème.

Je nettoie la plaque avant de la replacer sur l’imprimante.


![](../images/objetfinal.jpg)


Et voilà, notre porte-revus version miniature est terminé !




## Liens utiles

- [Site des imprimantes Prusa](https://www.prusa3d.com)
- [Communauté PrusaPrinters](https://blog.prusaprinters.org/#_ga=2.42844909.994300260.1634330638-22385276.1634330638)
- [Télécharger PrusaSlicer](https://www.prusa3d.com/prusaslicer/)
- [Modèle .stl](../images/Porte-revue_v1.stl))
- [Tutoriel Export format .stl depuis Fusion360](https://knowledge.autodesk.com/fr/support/fusion-360/troubleshooting/caas/sfdcarticles/sfdcarticles/FRA/How-to-export-an-STL-file-from-Fusion-360.html)



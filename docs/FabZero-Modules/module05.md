# 5. Usinage assisté par ordinateur

Pour cette dernière formation, nous avons reçu une formation à la Shaper Origin: c'est un outil d'usinage assisté par un ordinateur.


## La machine 


![](../images/shaperorigin.jpg)
*Photo issue du site [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec).*

La Shaper Origin est une _fraiseuse CNC portative de précision_, guidée à la main.

**Informations sur la machine :**

- Profondeur de découpe max. : 43 mm
- Diamètre du collet : 8 mm ou 1/8"
- Format de fichier supporté : SVG

Cette fraiseuse permet de découper des formes de manière très précise dans du bois grâce à une fraise dont la machine corrige la trajectoire lorsque l'on guide l'outil sur le matériau en suivant le tracé affiché sur l'écran.

Pour feuilleter le manuel d'utilisation, *[cliquez ici](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)*.


**Quelques précautions d'usage :**

- Utiliser la machine sur un plan de travail stable
- Toujours fixer le matériau sur le plan de travail à l'aide de double face, serre-joint ou vis
- Ne pas oublier d'allumer l'aspirateur à poussières
- Être dans une position stable, équilibrée et confortable pour l'utiliser, et veiller à ce que l'ensemble de la découpe soit à notre portée
- Éviter les vêtements larges, bijoux, ou cheveux longs lâchés


![](../images/schema.jpg) *Photo issue du site [Shaper Origin](https://www.shapertools.com/fr-fr/origin/spec).*


La machine se compose d'un corps principal avec une poignée pour la transporter. On y trouve un port usb pour importer notre fichier de découpe. Ensuite, les éléments qui coupent la fraise sur son mandrin. Un écran tactile au dessus nous permet de paramétrer et visualier la découpe. Deux poignées latérales pour prendre en main la machine pendant la découpe et où se situent de respectivement un bouton de démarage et un d'arrêt de découpe. Enfin, au dos, se trouve une caméra avec lumières LED pour se situer la machine sur le matériau. En effet, pour se situer la machine nécessite des repères : le ShaperTape.


![](../images/bandedominos.jpg)


Il faut placer des bandes parallèles de ce dernier tous les 8cm devant la machine qui utilise la caméra pour les scanner et pour définir notre espace de travail. Elle montre également en temps réel le placement de la découpe puis le tracé à suivre tout au long de celle-ci..

**Attention !** Il faut veiller à ce que le ShaperTape couvre toute la zone que l'on prévoit de découper et devant cette zone afin que la Shaper Origin capte toujours ces marqueurs.


## Réglages


La machine propose différentes vitesses allant de 1 à 6, qui correspondent à une vitesse de 10 000 à 26 000 tours/minute. Cette vitesse de fraisage dépend du matériau et de son épaisseur : une découpe trop lente ne sera pas lisse et laissera apparaître des à-coups tandis qu'une vitesse trop rapide brûlera ou écorchera le bois. Un indicateur de vitesse est la taille des copeaux lors de la découpe, la taille normale des copeaux est d'environ 1mm, s'ils sont plus gros c'est que la vitesse est trop lente, plus petits alors trop rapide. Elle se règle sur **le cadran de contrôle de vitesse**.

L'option de **vitesse automatique** doit également être déterminée et permet à la machine de guider elle-même la découpe, auquel cas il faut pouvoir la suivre malgré tout, on évitera donc des vitesses trop rapides.

Il existe plusieurs fraises de différentes tailles pour différents matériaux et épaisseurs.

*[Ici](https://support.shapertools.com/hc/fr-fr/articles/360016398434-Recommandations-de-paramètres-par-matériau)*, vous pouvez trouver **un tableau regroupant selon différents matériaux, la fraise, vitesse de la broche, vitesse auto et profondeur maximale adaptée**.

Il est important de prendre en compte l'épaisseur de la fraise dans la découpe et le tracé. 


![](../images/typedecoupe.jpg)
*Photo issue du [guide du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md).*


Comme montrer sur ce"tte photo, il existe différents types de découpes :

- *inside* : découpe à l'intérieur du tracé,
- *outside* : découpe à l'extérieur du tracé,
- *on line* : découpe sur le tracé
- *pocket* : coupe par évidage


![](../images/code.png)


## Utilisation

Voici quelques [tutos vidéos](https://www.shapertools.com/fr-fr/tutorials) mis en place par le fabriquant.

Pour expliquer le processus, je vais utilser la découpe qu'on a réalisé lors de la formation : une planète dans une planche de bois.


#### Le dessin

La machine, similairement à la découpeuse laser, nécessite une image vectorielle c'est à dire des images .svg. Il faut donc réaliser notre dessin sur un logiciel de tracés vectoriels comme Illustrator ou Inkscape.

***Attention ! Pour une découpe, assurez-vous que votre tracé soit bien fermé, sinon la machine n'est pas capable de faire une découpe complète de l'objet.***

#### La découpe

Afin de préparer la découpe il faut, comme expliqué plus haut, le matériau doit être bien fixer au plan de travail. 
Ensuite on place le ShaperTape comme expliqué avant (bandes parallèles séparées de 8cm max.et min. 4 rectangles) et enfin on met la fraise adaptée dans la machine.



_Pour changer la fraise, il faut tout d'abord appuyer le bouton de verrouillage de la broche, puis dévisser la vis à l'aide de la clé T-Hexagonale afin de retirer la broche._

Après avoir insérer la clé USB dans le slot, on clique sur *'nouveau scan'* dans le menu *'scan'* à droite de l'écran,. Il faut ensuite bouger la machine de façon à scanner tout le ShaperTape qui devient bleu quand il est reconnu.


![](../images/shaperscan.jpg)


Il est possible de dessiner des formes simples directement sur la machine depuis le menu 'dessiner'. Cet outil peut être utile pour effectuer des tests de calibrage de vitesse par exemple.

Pour accéder au dessin chargé sur la clé USB, on clique sur *'importer'*. Une fois notre fichier sélectionné, on peut visualiser son emplacement et la place qu'il prendra sur l'espace de travail. Si on le souhaite on peut changer son échelle, le tourner etc. Le curseur à droite permet d'agrandir ou rétrécir le point de vue sur l'espace de travail. Une fois satisfait de sa taille et emplacement, on clique sur *'placer'*.



**Attention !**
 En haut à droite de l'écran, un indicateur permettant de savoir si l'on se trouve dans l'espace de travail possible. Si c'est pas le cas, le voyant apparaît en _rouge_. S'il est en _gris et noir_ c'est qu'on y est presque, et lorsque l'on est bien placé, il s'affiche en _noir_.


Dans le menu de droite, on peut passer maintenant à l'étape 'fraiser'. Sur la gauche, on a les différents réglages : la profondeur de fraisage (ici sur 6mm), le type de découpe (ici sur outside), l'offset (ici à 0), la dimension de la fraise etc. On s'assure également que toute la découpe est à notre portée et que l'on soit dans une position stable pendant la découpe. Lorsque tous les paramètres sont enregistrés, on clique sur _"fraiser"_.


![](../images/fraisage.jpg)


Avant d'effectuer la découpe, il ne faut pas oublier d'allumer **l'aspirateur à poussières** et de le relier à la machine à l'aide du tuyau adaptateur, et d'allumer également la broche grâce au bouton **on/off**.


![](../images/onoff.jpg)


Pour lancer la découpe, on appuie une fois sur le **bouton vert** sur la poignée de droite. Si on reste appuyé, la machine passe en vitesse automatique.

On suit la trajectoire affichée sur l'écran en suivant le sens de la flèche. La partie du tracé déjà découpée apparaît alors en **bleu**. Si le cercle blanc sort du tracé, la machine arrête automatiquement la découpe et le mandrin se relève. Pour arrêter la découpe soi-même, on appuie sur le **bouton rouge** sur la poignée de gauche.


![](../images/decoupe.jpg)


_**Important!**_ On ne peut pas enchainer les découpes de formes disctinctes. Entre chaque forme ou tracé différent, il faut arrêter le fraisage et relever le mandrin (bouton rouge).

Une fois la découpe terminée, il faut bien éteindre la broche en poussant le bouton sur **off**.


![](../images/hexagonep.jpg)


Malheureusement pour nous, le dessin était trop compliqué à faire avec la Shaper car les formes de dessins étaient très petites et la vitesse de la toupie trop compliquée à configurer sur un si petit modèle. 

Voici cependant une photo du résultat de l'autre groupe qui donne un aperçu de ce qu'on peut faire avec la Shaper! 


![](../images/objetfinalmonde.jpg)



## Liens utiles

- [Manuel d'utilisation](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)
- [Tutoriels vidéos](https://www.shapertools.com/fr-fr/tutorials)
- [Guide d'utilisation du FabLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md)








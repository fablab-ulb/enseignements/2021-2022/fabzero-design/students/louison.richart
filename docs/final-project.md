# Final Project


## Etape 1


Au cours des dernières semaines, nous avons travaillé sur notre idée de projet final. Dans le cadre du Module 3, je participe à la conception d'un projet qui tente de faire découvrir le design aux enfants et de les intéresser davantage au sujet. Nous travaillons en partenariat avec le Brussels Design Museum, où se déroule ce projet.
Pour commencer, nous avons divisé les groupes. Chaque groupe travaille sur un projet qui attirera l'attention des enfants sur les objets exposés dans le musée. Notre groupe, composé de Camila, Louise et moi, avons décidé de travailler avec des miniatures. 

Nous pensons qu'elles peuvent beaucoup parler aux enfants car ils pourront enfin toucher les objets qu'ils ont vus lors de leur visite. De plus, il est possible d'y voir des détails de plus près, de comprendre ses formes et aussi, c'est une taille idéale pour un jeu par exemple.
Nous avons donc décidé de rechercher, modéliser et créer ces miniatures avec les imprimantes 3D disponibles sur Fablab. 
C'est le point de départdu développementde notre jeu de société pédagogique



La première semaine de travail, nous savions que nous voulions travailler avec les miniatures, mais que pourrions-nous développer d'autre de manière à ce que les enfants puissent réellement apprendre quelque chose d'utile?

L'objectif étant de créer un jeu éducatif et ludique. On avait quelques idées en tête: travailler sur les couleurs, les textures, le toucher,...
Il fallait donc voir ce qui fonctionnerait le mieux dans un but pédagogique.
L'idée première était d'utiliser des cartes qui représentaient graphiquement les différentes caractéristiques des objets du musée que nous aurions imprimés en 3D. Les cartes seraient alors, en correspondance avec l'une des miniatures.


![](images/cartes1.jpg)


L'idée de travailler avec les caractéristiques nous semblaient très intéressantes car les enfants pourront travailler sur leur mémoire, leur concentration et leur compréhension de la visite. Nous devions encore décider combien et quels objets nous utiliserions pour le jeu sous forme de miniature. Il serait impossible de tous les passer en revue dans un jeu, alors nous avions déjà en tête d'imprimer envion 10 objets. 


## Etape 2 

La deuxième semaine, nous avons créé une liste des objets choisis : nous avons essayé de sélectionner un objet « iconique » de chaque pièce/podium du musée et aussi de chaque plastique: nous sommes arrivés à 8 objets. Ces objets étant les plus attrayants à nos yeux. 

Ensuite, nous avons commencé à modéliser ces objets sur Fusion et à les imprimer sur les machines Fablab 3d. Certains fichiers ont pris jusqu'à 12 heures pour s'imprimer. Nous avons eu quelques impressions qui ont échoué mais finalement au redémarrage, cela a fonctionné.
Nous en avons pour l'instant 6, cela nous semblait suffisant pour un 1er test avec les enfants. 
Les objets que nous avons choisis et imprimés sont les suivants, à côté de son emplacement dans le musée et de ses caractéristiques :

| Endless Flow | Dirk Vander Kooij  |
| ------ | ------ |
| ![](images/endlessflow.jpg) | Podium "recyclés" - RECYCLE.   Caractéristiques : Recyclé - Frigo - Impression 3D |

| Cantilever | Verner Pantone  |
| ------ | ------ |
| ![](images/cantilever.jpg) | Podium “porte à faux".   Caractéristiques : Prouesse technique - resistant - 2 pieds (porte-à-faux) |

| Chica | Jonathan de Pas, Donato d'Urbino, Paolo Lomazzi, Giorgio de Curso   |
| ------ | ------ |
| ![](images/chica.jpg) | Première salle - ABS.   Caractéristiques : Modulaire - Empillable - Pour enfants |

| Pratone | Pietro Derossi, Giorgio Ceretti et Riccardo Rosso   |
| ------ | ------ |
| ![](images/pratone.jpg) | Podium "radical design" - PUR.   Caractéristiques : Mou - Gigantisme - Détournement |

| Translation | Alain Gilles  |
| ------ | ------ |
| ![](images/translation.jpg) | Podium "recyclés" - RECYCLE.   Caractéristiques : Recyclé - Composé de plein de petits objets - Multicolore |

| Dondolo | Cesare Leonardi et Franca Stagi  |
| ------ | ------ |
| ![](images/dondolo.jpg) | Salle “conquête spatiale” – GRP (fibre de verre).   Caractéristiques : Futuriste/espace - Chaise à bascule - Anti-gravité |




Nous avons ensuite essayé de repenser les cartes afin de les rendre plus accessibles aux enfants en y ajoutant des couleurs.

![](images/cards1.jpg)


## Etape 3 

Nous avons améliorer le jeu pour qu’il soit « autonome » : où n'importe qui pourrait jouer même si nous n'étions pas présents. Nous avons donc réfléchi à un scénario.

Nous avons pensé que ce serait bien d'utiliser le plan du musée comme planche, puisque nous utilisions également les objets en miniature, il était logique de les assembler et de créer un jeu de plateau du musée miniature. Le jeu fonctionne comme un jeu de plateau typique : il y a des pions pour chaque joueur, des dés et le plateau avec un chemin à suivre pendant que vous jouez. Il compte également avec des cartes, qui sont divisées en 3 catégories : histoire, caractéristiques et bonus/curiosités. Chaque catégorie correspond à une couleur qu’on retrouve sur les cases du plateau. 


#### Cartes


Sur les cartes nous avons également pensé à ajouter 3 options de réponse afin que le jeu soit plus facile pour l’enfant. 

Voici les cartes colorées que nous avons conçues et imprimées :


![](images/verta.jpg)
![](images/histoirea.jpg)
![](images/bonus.jpg)


Voici le résultat des cartes : 

![](images/cartes2.jpg)


#### Récompense

Pour le prix, nous avons modélisé des « granulés » et les avons imprimés en 3D. A chaque bonne réponse, l’enfant reçoit un point. A la fin, une fois tous les granulés pris, la classe peut obtenir un prix commun ! Cela renforce l'esprit d'équipe et cela pourrait être une « compétition » plus saine pour les enfants, sachant que tout le monde y contribue.


![](images/boitegranules1.jpg) ![](docs/images/granules.jpg)

#### Pions

Ci-dessous, nous avons les pions qui représentent les différents designers qui ont conçu les objets sélectionnés plus haut. 


![](images/pions.jpg)


#### Plateau et règles du jeu

Le plateau a été réalisé à l'aide d'un programme informatique de dessin. Pour le 1er test, il est imprimé en papier mais pour sa version finale, nous pensons à une structure en bois où nous pourrions graver et colorer les différentes cases du jeu.

On compte aussi faire différentes boites pour mettre les composants du jeu et avoir le tout organisé dans une grande boite.


Voici le scénario et les règles du jeu que nous allons essayer cette semaine avec les enfants :


![](images/reglesdujeu.jpg) 


![](images/plateautest.jpg)



### Visite des enfants au FabLab


![](images/jeuenfants1.jpg) ![](docs/images/jeuenfants2.jpg)




## DEBRIEFING AFTER CRASH TEST


| Réussite | A améliorer  |
| ------ | ------ |
|Le jeu en lui-même a bien fonctionné : l'idée « d'être dans le musée » dans le jeu | Imprimer les objets dans leur vraie couleur (comme dans le musée) |
|Nombre de cases du jeu et de cartes idéal | Afficher le nom à côté l'objet  |
|Enfants curieux, se souvenaient de la visite et ont appris de nouvelles choses | + de cartes vertes (caractéristiques)  |
|L'entraide, l'esprit d'équipe recherché -> TOP| Changement sur les règles : carte verte 1 point, bleue 2, rouge 3  |
|Système de récompenses (granulés) les poussent à se dépasser ou réfléchir davantage | Boîtes de rangement et design du plateau (3 a1 de bois pour le plateau, améliorer les cartes et les pions 3D -> + résistants) |
| | Prix: objets miniatures de poche à donner à la fin du jeu |
| | Retravailler le plateau de jeu -> + design et complexe |


## Amélioration pour le pré-jury décembre

### Plateau
Pour le plateau de jeu, la plus grosse difficulté était de trouver une solution pour le plier mais sans l'endommager.

**Solution trouvée :** système de puzzle à assembler. Les enfants doivent ainsi construire le plateau de jeu avant de pouvoir y jouer (travail sur la mémoire visuelle et spatiale).

Nous avons créé de grandes pièces de puzzle découpées avec Laserseur qui se recollent les unes aux autres pour former le plan du musée.

On a dessiné le plan avec ses divisions de puzzle, afin que nous puissions nous assurer de quelles tailles de « carton » nous avons besoin. Ensuite, nous avons passé notre fichier à SVG pour pouvoir découper avec la machine laser. Nous avons fait tous les ajustements d'échelle avant et heureusement, tout a bien fonctionné !


![](images/plateaupuzzle.jpg)

![](images/puzzledecoupe.jpg)


Le plan a été imprimé en papier satiné , et une fois les pièces du puzzle/plan découpées, on a découpé le plan pour le coller dans chaque pièce correspondante. 
Voici le résultat :


![](images/satine.jpg)




### Cartes

Des modificartions ont été faites sur certaines questions, en donnant quelques conseils aux enfants pour trouver la bonne réponse. Nous avons également additionné certaines des cartes vertes, car nous avons constaté qu'elles n'étaient pas suffisantes la dernière fois. Celles-ci ont été réimprimées dans un papier plus rigide. 


### Miniatures

Pour les miniatures, nous avons dû en réimprimer certaines pour les avoir aux couleurs d'origine des objets du musée, rendant le jeu le plus proche possible de la réalité. Malheureusement, il reste encore une chaise à imprimer dans sa vraie couleur, la Cantilever. L'impression a échoué plusieurs fois donc il va falloir la redémarrer... 


![](images/a5.jpg)


Pour les miniatures : nous avons aussi commencé à imprimer des mini versions de nos miniatures.
Ce sont les prix que nous remettrons à chaque groupe d'enfants qui jouent au jeu ! 



 ### Pions

Pour les pions nous avons décidé de les imprimer également en 3D pour une meilleure rigidité et qu'ils soient visuellement plus attractif.

Ils ont été modélisé sur Fusion 360 et imprimer sur machine 3D.

![](images/pionsfusion.jpg)

Voici le rendu des pions : 


![](images/pions1.jpg)


### Boîte de jeu



Un beau jeu passe aussi par son enveloppe, si la boite est belle et actrative, le jeu gagne en crédibilité. 

J'ai décidé de la fabriqué chez moi dans l'atelier de mon père qui est ébéniste. 
 
La boîte est en bois, découpée à la scie sicrulaire sur table. La boite mesure 50x40x15 dans le nut d'y ranger le plateau, les pios et toutes les miniatures. Voici rapidement comment j'ai fonctionné pour la construction de la boite :  


![](images/sciecirculaire.jpg)
![](images/boiteinter1.jpg)     ![](images/boiteinter2.jpg)


Cette boîte est évidement temporaire car c'est un prototype au bonne dimension. Pour le jury final, elle comportera sûrement des gravures, un intérieur plus travaillé et adapté au rangement du jeu et l'extérieur poncé et vernis. 



## PREJURY 

![](images/prejury.jpg) ![](images/prejurypions.jpg) ![](images/prejurychica.jpg) ![](images/prejuryjetons.jpg)


Tout s'est bien passé pour les présentations ; nous avons passé la journée à partager nos idées avec les membres du jury et avons pris note de certaines choses que nous pouvions encore améliorer pour le jury final. Nous étions sur la bonne voie et les améliorations étaient visibles. Maintenant, nous devons nous concentrer sur la partie conception/design et continuer à l'améliorer pour obtenir un meilleur résultat !


## Feed back pré-jury décembre et amélioration pour le jury final


**Le + important :** Améliorer le plateau de jeu -> le rendre + attractif, moins 2D, + vivant et graphique (travailler sur la matérialité?)

- Pions : Changer la forme -> trouver une forme + stable et + design (s'inspirer de pions de vrai jeu de plateau)
Donner davantage d'informations sur les designers choisis.

- Cartes : Modifier quelques cartes caractéristiques (vertes) -> pictogrammes plus homogènes et moins complexes pour certaines cartes
Revoir certains termes employés pour qu'ils soient compréhensibles par les enfants.

- Récompense et boite de récompense : trouver quelque chose de plus design et attractif. Les pions doivent être plus parlant et en rapport avec notre projet. Récompenses -> autocollants ? 

- Règle du jeu à retravailler légèrement.

- Boite du jeu : réduire son poids parce qu'elle était un peu trop lourde -> bois épais de 2,2cm



## Amélioration et finalisation du projet


### Plateau

Le plateau était une partie importante à travailler, car il constitue une grande partie du jeu et il a un poids visuel. Nous avons donc décidé de commencer par modifier quelques détails, comme le parcours des cases du jeu. Nous avons changé les cases carrées en cases arrondies, ce qui donne un meilleur visuel en général. Nous avons conservé les 3 couleurs : rouge, bleu et vert, car elles ont eu une réaction positive de la part du pré-jury ainsi que de la part des enfants. Les couleurs sont un bon moyen de communication pour eux. Nous avons également changé la police de caractères pour utiliser la même que celle au Musée du design de Bruxelles. Nous avons veillé à utiliser cette même police sur tous les autres éléments du jeu: les cartes, les règles... afin de conserver une homogénéité. 

![](images/plateaufinalautocad.jpg)


Nous avons aussi modifié la matérialité du plateau qui est maintenant une bache, qui est en une seule pièce et qu'on peut rouler comme un "plateau de casino". Cela pourrait être plus simple que l'idée du puzzle, qui demanderait probablement beaucoup de temps pour le début du jeu pour les enfants. Ce matériau offre également une meilleure qualité (on reste dans l'esprit du plastique) et une sensation de toucher plus agréable.


Après ces changements, nous avons réalisé qu'il serait intéressant d'ajouter quelque chose de plus au tableau, pour que les enfants comprennent l'usage d'une certaine chaise, le matériau utilisé, etc. Nous avons ajouté de petites photos des chaises ou de quelque chose en rapport direct avec elles juste sous l'objet correspondant. Nous avons ajouté de petites photos des chaises ou d'autres objets.
Nous avons placé l'image juste en dessous du podium de l'objet dans le plateau, afin qu'elle puisse également ressembler à une petite "affiche", comme c'est le cas dans le musée, où l'on voit les informations juste à côté de l'objet. 



![](images/plateaufinalimprime.jpg) ![](images/photopratone1.jpg) 


Un élément important du plateau que nous avons travaillé est le système de podiums. En effet, nous voulions donné plus de volume au plateau. Nous avons ainsi modéliser les 5 podiums et les avons imprimer à l'imprimante 3D. Ceux-ci font 1,6cm de hauteur surlequel vient s'ajouter 4mm de plexiglass gravé représentant les différents objets vu en 2D. On peut comme cela retrouvé leur place facilement lorsqu'on installe le jeu tout en ajoutant un coté esthétique et du volume au plateau. 
Deux podiums ont été fabriqué en 2 parties puis emboité l'un dans l'autre car ils ne rentraient pas entièrement sur la plateau d'impression 3D. 

![](images/module2parties.png) ![](images/podiumsnoirs.jpg) ![](images/plexis.jpg) ![](images/plexisnoir.jpg) 




### Cartes


Le processus de création des cartes n'a pas été facile, car il a été assez difficile de trouver un site Web proposant des pictogrammes de même style. 

Nous avons commencé par sélectionner ceux qui étaient un peu trop confus et difficillement identifiable et les avons supprimés ou mis une double réponse. Nous avons également décidé de mettre le même nombre de cartes pour chaque couleur = 12 pour chaque,36 au total. Cela est cohérent avec le jeu puisqu'au test avec les enfants, les cartes n'étaient jamais toutes utilisées. 


![](images/carteverte9.jpg) 

![](images/cartevertesuite.jpg)  


### Objets miniatures 


 La Cantilever (rouge) et la Dondolo (blanc) ont été imprimé dans leur vraie couleur et cette fois sans accros d'impression. 
  
![](images/cantelever9.jpg) ![](images/dondoloprint.jpg)  

### Pions


Un gros travail de recherche a été fait sur les pions: nous voulions garder l'idée de représenter les pions du jeu par les designers des 6 diférentes chaises mais il fallait changer l'esthétique et le principe des pions.

Nous avons d'abord conceptualiser un pion avec un clip pour raccorder un carton rectangulaire de 3mm. Cependant, cette tentative faisait du pion un élément trop grand et pas très stable. Nous avons ainsi juste garder la base ronde avec le clip permettant d'attacher le morceau de carton où est imprimé au recto: une photo du designer et au verso: le nom de celui-ci et la photo + le nom de sa chaise. 


![](images/testpionsnul.jpg) ![](docs/images/impressionpions2a.jpg) 



![](images/cartesrectoverso.jpg)



**_Voici le résultat final obtenu:_**


![](images/pionstete.jpg)



### Boite de récompense 

L'intention est restée identique qu'au préjury: créer une boite de récompense dans laquelle les enfants peuvent prendre un jeton à chaque bonne réponse tout en ayant une partie inférieure avec un "trésor" caché. Lorsque le jeu est fini ou que les enfants ont récolté tous les jetons dans la partie supérieure, ils peuvent tiré la trappe du bas et découvrir leur cadeau. -> fabriquée avec imprimante 3D


![](images/boiterecomp3d2.jpg)![](images/boiterecomporange2.jpg) ![](images/boiterecomporange.jpg)



#### Jetons et autocollants (surprise)


Pour les **points/jetons**, nous voulions créer quelque chose de plus personnalisé pour le jeu et qui avait un lien direct avec le Musée du Design. Nous avons pris un morceau d'une plaque de plastique dur d'environ 2 mm et de couleur crème. 


![](images/jetonsblanc.jpg) ![](images/boiteorange.jpg) 



Pour les fabriquer, nous avons utilisé l'Epilog. Nous avons dû choisir une pièce pour la découper (vecteur) dans le format des pièces et ensuite, pour les personnaliser, nous avons utilisé la "gravure". Les gravures sont des dessins des chaises utilisées dans le jeu, fait sur autocad.

Voici quelques uns des paramètres utilisés sur l'Epilog :


![](images/parametresjetons2a.jpg) ![](images/parametresjetons.jpg)



**Pour les récompenses** à la fin du jeu, nous avons modelé des autocollants. Nous avons pensé que ce serait une façon amusante pour les enfants d'avoir un souvenir en rapport avec la visite. Pour les autocollants, nous avons préféré utiliser les photos des objets pour avoir la couleur et la texture telles qu'elles sont en réalité. Voici ce que cela donne :


![](images/autocollants.jpg)


### Règles du jeu


Imprimées en format A5 "livret". Le papier choisi est plus épais que le précédent. Il s'agit d'un papier spécial aussi utilisé pour les règles de jeux de société. Par rapport à la première version, celle-ci a une couverture et les informations sont plus clairement divisées.


![](images/reglescoverzz.jpg) ![](images/regleszz.jpg) 



### Boite packaging


Elle a été faite avec 2 bois différents de même épaisseur (2mm): chêne plaqué ONG (un coté foncé et l'autre de couleur naturelle) et sapeli.   Elle a été dimensionné pour qu'elle soit la plus petite possible tout en ayant la possibilité d'y ranger la totalité du jeu. La partie supérieure (le couvercle) a été étudié pour coulisser. 


![](images/sketchup1.jpg)

![](images/sketchup2.jpg)      ![](images/sketchup4.jpg)





La structure est la même que la précédente avec les coins coupés à 45° sur une scie à panneaux -> pour ne pas voir les champs. Les planches ont ensuite été passé dans une toupie pour retirer 2 bandes de 5mm pour pour incorporer le fond dans le caisson et faire coulisser le couvercle. Elles ont finalement été poncé et vernis. 





![](images/toupie2.jpg) ![](images/vernis.jpg) 
![](images/collagecoins.jpg) ![](images/serrage.jpg)



L'intérieur de la boite est en bois sapeli et fait 1,1mm d'épaisseur. Les 4 planches sont collées et vissées entre elles pour faire une structure indépendante, elle peut donc s'enlever si besoin (changement disposition intérieur boite,...)



![](images/structureind.jpg) ![](images/boitecomplet.jpg) 

Il ne restait plus qu'à inclure une poignée pour qu'elle soit transportable. 

![](images/boiteferme1.jpg) 


# Résultat final 


![](images/finjeua.jpg) 



![](images/finalllla.jpg) 
















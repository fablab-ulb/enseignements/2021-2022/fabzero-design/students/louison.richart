## A propos de moi

![](images/Louison.jpg)

Hello ! Je m'appelle **Louison Richart**. J'ai 22ans et je suis actuellement en Master 1 en architecture dans la faculté La Cambre-Horta de Bruxelles.

Je suis quelqu'un de très curieux et bricoleur, d'où mon intérêt pour l’option _Architecture & Design_.

Ici je détaillerai mon parcours personnel, mais aussi mon apprentissage tout au long de l’option Design.



## Mon histoire

Je suis issu de 7 générations de parents travaillant le bois : charpentier, menuisier et ébéniste. J'ai donc toujours baigné dans un milieu de manuels et de bricoleurs amoureux du bois et des casses têtes que peuvent engranger leurs métiers.  

A la fin de mes humanités, j'ai longtemps hésité entre les études d'ingénieur civil/industriel ou bien l'architecture. Deux études qui me semblaient à la fois très liées mais également très différentes. Je me suis finalement penché vers l'architecture qui est pour moi un condensé d'énormément de sujets qui m'intéressent et qui apportent de nombreux débouchés différents.



## Mes objectifs

* Développer mes compétences de modélisations 3D
* Apprendre à utiliser les différentes machines du Fablab
* Développer des compétences et connaissances en design



### Le projet : _Les enfants au musée Design de Bruxelles_

Pour moi, le design est de façon générale assez compliqué à définir. Essayer de l'expliquer clairement et simplement à des enfants est donc un beau défi.

Mon père est un grand intéressé de design depuis son plus jeune âge et a réussi à me transmettre l'émotion ou la passion qu'il avait pour certaines oeuvres ou artistes. L'objectif selon moi pour ses enfants, est exactement le même que celui d'un père qui explique les choses à son fils.

De plus, nous (12 étudiants) allons travailler avec le personnel du musée,  c'est pour moi une superbe occasion de travailler en équipe et de développer au maximum notre intérêt pour le design et nos capacités à travers différents exercices/méthodes d'apprentissages pour les enfants.

Le but de ce projet est de leur construire un espace ludique où ils auront la possibilité de s'amuser tout en apprenant.






